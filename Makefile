lint:
	pylint *.py

test-unit:
	python3 -m pytest *.py -W ignore -v
