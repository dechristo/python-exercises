class Exercise11:

    def __init__(self, collection):
        self.collection = collection

    def hasCommonElement(self, sub_collection):
        commons = []
        commons = list(filter(lambda x: x in self.collection, sub_collection))
        return True if commons else False


    def hasCommonElementLC(self, sub_collection):
        '''using list comprehension, thus LC at the end'''

        commons = []
        commons = [item for item in sub_collection if item in self.collection]
        return True if commons else False

