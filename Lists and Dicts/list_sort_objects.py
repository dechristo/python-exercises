from src.model.discipline import Discipline

disciplines = []
disciplines.append(Discipline('history' ,89))
disciplines.append(Discipline('math' ,100))
disciplines.append(Discipline('english' ,98))
disciplines.append(Discipline('geography' ,55))
disciplines.append(Discipline('chemistry' ,87))
disciplines.append(Discipline('physics' ,100))

disciplines.sort(key=lambda discipline: discipline.grade)

for discipline in disciplines:
    print(discipline.name , discipline.grade, sep=': ')
