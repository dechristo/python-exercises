import operator
from functools import reduce
objects = []

obj1 = {
    'name' : 'a',
    'scores' : [10, 10, 10, 9]
}
objects.append(obj1)

obj2 = {
    'name' : 'b',
    'scores' : [8, 7, 8, 9]
}
objects.append(obj2)

obj3 = {
    'name' : 'c',
    'scores' : [10, 4, 9, 6]
}
objects.append(obj3)

obj4 = {
    'name' : 'd',
    'scores' : [7, 7, 7,1000]
}

averages = {score:avg for (score,avg) in objects}

sorted_objects = sorted(objects, key = lambda v: reduce((lambda x, y: y + y), v['scores'])/len(v['scores']))
#print(sorted_objects)

dict_name_avg = {}

for obj in objects:
    dict_name_avg[obj['name']] = reduce((lambda x, y: x + y), obj['scores'])/len(obj['scores'])

print(sorted(dict_name_avg.items(), key=operator.itemgetter(0), reverse=True))
