class Exercise6:

    def __init__(self):
        pass

    @staticmethod
    def sort_by_last_element(tuples):
        sorted_tuples = sorted(tuples, key=lambda x: x[-1])
        return sorted_tuples


    @staticmethod
    def sort_by_last_element_in_place(tuples):
        tuples.sort(key=lambda x: x[-1])
        return tuples
