class RemoveDuplicates:

    def __init__(self, collection):
        self.collection = collection


    def remove_duplicates_using_set(self):
        return list(set(self.collection))


    def remove_duplicates(self):
        unique = []

        for x in self.collection:
            if x not in unique:
                unique.append(x)

        return unique

    def remove_duplicates_comprehension(self):
        unique = []

        [unique.append(x) for x in self.collection if x not in unique]
        return unique
