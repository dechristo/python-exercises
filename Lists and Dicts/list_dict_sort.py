array_test = []

obj1 = {'id' : 34}
array_test.append(obj1)

obj2 = {'id' : 340}
array_test.append(obj2)

obj3 = {'id' : 10}
array_test.append(obj3)

print(sorted(array_test, key=lambda x: x['id']))
