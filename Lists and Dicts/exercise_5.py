class Exercise5:

    def __init__(self, words):
        self.words = words


    def count(self, size):
        filtered = list(filter(lambda x: len(x) >= size and x[0]==x[-1] , self.words))
        return filtered
