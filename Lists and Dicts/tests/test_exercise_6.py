import unittest
from exercise_6 import Exercise6

class Exercise6Test(unittest.TestCase):

    def test_return_sorted_list_of_tuples(self):
        tuples =  [(2, 5), (1, 2), (4, 4), (2, 3), (2, 1)]
        expected = [(2, 1), (1, 2), (2, 3), (4, 4), (2, 5)]

        sorted_tuples = Exercise6.sort_by_last_element(tuples)

        self.assertEqual(expected, sorted_tuples)

    def test_return_sorted_list_of_tuples_in_place(self):
        tuples =  [(2, 5), (1, 2), (4, 4), (2, 3), (2, 1)]
        expected = [(2, 1), (1, 2), (2, 3), (4, 4), (2, 5)]

        Exercise6.sort_by_last_element_in_place(tuples)

        self.assertEqual(expected, tuples)

