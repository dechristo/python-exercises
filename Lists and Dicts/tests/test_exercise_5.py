import unittest
from exercise_5 import Exercise5

class Exercise5Test(unittest.TestCase):

    def test_find_words_with_length_greater_or_equal_than_2_and_equal_first_last_letters(self):
        words = ['abc', 'xyz', 'aba', '1221']
        self.word_counter = Exercise5(words)
        filtered = self.word_counter.count(2)

        self.assertEqual(len(filtered), 2)
        self.assertEqual(filtered, ['aba', '1221'])



    def test_find_words_with_length_greater_or_equal_than_3_and_equal_first_last_letters(self):
        words = ['aba', 'xyxy', 'fdserrf', 'aaaaaaaaaaa', '3453665653', '245565656789', '$sdfdsfd$', '#d#', 'tzrztgg']
        self.word_counter = Exercise5(words)
        filtered = self.word_counter.count(3)

        self.assertEqual(len(filtered), 6)
        self.assertListEqual(filtered, ['aba', 'fdserrf', 'aaaaaaaaaaa', '3453665653', '$sdfdsfd$', '#d#'])
