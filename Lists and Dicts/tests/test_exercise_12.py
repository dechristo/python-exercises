from exercise_12 import Exercise12
import unittest


class Exercise12Test(unittest.TestCase):
    def test_returns_none_if_list_size_lesser_than_max_index(self):
        exercise_12 = Exercise12()
        result = exercise_12.remove_elements([1, 2, 3])

        self.assertIsNone(result)


    def test_remove_0_4th_and_5th_indexes_returns_correct_list(self):
        exercise_12 = Exercise12()
        result = exercise_12.remove_elements([1, 2, 3, 4, 5, 6])
        expected = [2, 3, 4]

        self.assertEqual(result, expected)
