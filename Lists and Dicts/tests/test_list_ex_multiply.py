import unittest
from src.base.list_ex import ListEx

class LibExMultiplyTest(unittest.TestCase):

    def test_multiply_reduce_lambda_returns_correct_value_for_positive_integers_greater_than_zero(self):
        ints = [8, 2, 1, 3, 4, 5]
        self.list_ex = ListEx(ints)
        result = self.list_ex.multiply_reduce_lambda()

        self.assertEqual(result, 960)


    def test_multiply_reduce_lambda_returns_zero_if_zero_in_list(self):
        ints = [8, 2, 1, 3, 0, 4, 5]
        self.list_ex = ListEx(ints)
        result = self.list_ex.multiply_reduce_lambda()

        self.assertEqual(result, 0)


    def test_multiply_reduce_returns_correct_value_for_positive_integers_greater_than_zero(self):
        ints = [1, 77, 300, 56, 44, 11, 9, 1900]
        self.list_ex = ListEx(ints)
        result = self.list_ex.multiply_reduce()

        self.assertEqual(result, 10706351040000)


    def test_multiply_reduce_returns_zero_if_zero_in_list(self):
        ints = [8, 2000, 112, 3, 20, 4, 5, 0]
        self.list_ex = ListEx(ints)
        result = self.list_ex.multiply_reduce_lambda()

        self.assertEqual(result, 0)


    def test_multiply_reduce_recursive_returns_correct_value_for_positive_integers_greater_than_zero(self):
        ints = [10, 20, 30, 40, 50, 2, 2, 2]
        self.list_ex = ListEx(ints)
        result = self.list_ex.multiply_recursive(0, 0)

        self.assertEqual(result, 96000000)


    def test_multiply_recursive_returns_zero_if_zero_in_list(self):
        ints = [1, 0, 444, 35, 82364]
        self.list_ex = ListEx(ints)
        result = self.list_ex.multiply_reduce_lambda()

        self.assertEqual(result, 0)
