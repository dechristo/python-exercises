import unittest
from src.base.list_ex import ListEx

class SmallesttNumberListTest(unittest.TestCase):

    def test_find_smallest_on_positive_integers_list(self):
        ints = [3, 55, 9, 84, 34, 333, 12314, 0, 5, 145]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_smallest(), min(ints))


    def test_find_smallest_on_negative_integers_list(self):
        ints = [-3, -55, -9, -84, -34, -333, -12314, -5, -145]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_smallest(), min(ints))


    def test_find_smallest_on_equal_integers_list(self):
        ints = [3, 3, 3, 3, 3]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_smallest(), min(ints))


    def test_find_smallest_on_integers_list(self):
        ints = [-3, 55, 9, -84, 34, 333, -12314, 0, 5, 145]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_smallest(), min(ints))

    def test_find_smallest_on_positive_integers_list_recursively(self):
        ints = [3, 55, 9, 84, 34, 333, 12314, 0, 5, 145]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_smallest_recursive(ints[0], 1), min(ints))
