import unittest
from src.base.list_ex import ListEx

class ListExSumTest(unittest.TestCase):

    def setUp(self):
      pass


    def test_sum_reduce_returns_correct_sum_of_list_for_positive_integers(self):
        ints = [3, 5, 2, 556, 124, 0, 100, 4]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_reduce()

        self.assertEqual(result, 794)


    def test_sum_reduce_lambda_returns_correct_sum_of_list_for_positive_integers(self):
        ints = [33, 67, 450, 50, 1000, 4, 7, 1, 24]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_reduce_lambda()

        self.assertEqual(result, 1636)


    def test_sum_recursive_returns_correct_sum_of_list_for_positive_integers(self):
        ints = [3, 1, 44, 678, 13456, 780]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_recursive(0, 0)

        self.assertEqual(result, 14962)


    def test_sum_reduce_returns_correct_sum_of_list_for_negative_integers(self):
        ints = [-3, -5, -2, -556, -124, -100, -4]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_reduce()

        self.assertEqual(result, -794)


    def test_sum_reduce_lambda_returns_correct_sum_of_list_for_negative_integers(self):
        ints = [-33, -67, -450, -50, -1000, -4, -7, -1, -24]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_reduce_lambda()

        self.assertEqual(result, -1636)


    def test_sum_recursive_returns_correct_sum_of_list_for_negative_integers(self):
        ints = [-3, -1, -44, -678, -13456, -780, 0, 0, 0]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_recursive(0, 0)

        self.assertEqual(result, -14962)


    def test_sum_reduce_returns_correct_sum_for_list_of_equal_positive_integers(self):
        ints = [2, 2, 2, 2, 2, 2, 2]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_reduce()

        self.assertEqual(result, 14)


    def test_sum_reduce_lambda_returns_correct_sum_for_list_of_equal_positive_integers(self):
        ints = [7, 7, 7, 7, 7, 7, 7]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_reduce_lambda()

        self.assertEqual(result, 49)


    def test_sum_recursive_returns_correct_sum_for_list_of_equal_positive_integers(self):
        ints = [2567, 2567, 2567, 2567, 2567, 2567, 2567]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_recursive(0, 0)

        self.assertEqual(result, 17969)


    def test_sum_reduce_returns_correct_sum_for_list_of_equal_negative_integers(self):
        ints = [-2, -2, -2, -2, -2, -2, -2]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_reduce()

        self.assertEqual(result, -14)


    def test_sum_reduce_lambda_returns_correct_sum_for_list_of_equal_negative_integers(self):
        ints = [-77, -77, -77, -77, -77, -77, -77]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_reduce_lambda()

        self.assertEqual(result, -539)


    def test_sum_recursive_returns_correct_sum_for_list_of_equal_negative_integers(self):
        ints = [-1, -1, -1, -1, -1, -1, -1]
        self.list_ex = ListEx(ints)
        result = self.list_ex.sum_recursive(0, 0)

        self.assertEqual(result, -7)
