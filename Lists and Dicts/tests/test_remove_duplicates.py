import unittest
from exercise_7 import RemoveDuplicates


class RemoveDuplicatesTest(unittest.TestCase):

    def test_list_of_ones_return_one(self):
        ones = [1, 1, 1, 1, 1, 1]
        remove_duplicates = RemoveDuplicates(ones)

        self.assertEqual(len(remove_duplicates.remove_duplicates()), 1)
        self.assertListEqual(remove_duplicates.remove_duplicates(), [1])
        self.assertEqual(len(remove_duplicates.remove_duplicates_using_set()),1)
        self.assertListEqual(remove_duplicates.remove_duplicates_using_set(), [1])
        self.assertEqual(len(remove_duplicates.remove_duplicates_comprehension()), 1)
        self.assertListEqual(remove_duplicates.remove_duplicates_comprehension(), [1])


    def test_list_of_repeated_elements_return_uniques(self):
        ones = [1, 156, 2, 2, 77, 5, 1, 23, 689, 0, 433, 156, 1000, 5, 77, 77, 77, 99]
        expected = [1, 156, 2, 77, 5, 23, 689, 0, 433, 1000, 99]
        expected.sort()
        remove_duplicates = RemoveDuplicates(ones)
        result = remove_duplicates.remove_duplicates()
        result.sort()

        self.assertEqual(len(remove_duplicates.remove_duplicates()), 11)
        self.assertListEqual(result, expected)

        result = remove_duplicates.remove_duplicates()
        result.sort()
        self.assertEqual(len(remove_duplicates.remove_duplicates_using_set()),11)
        self.assertListEqual(result, expected)

        result = remove_duplicates.remove_duplicates()
        result.sort()
        self.assertEqual(len(remove_duplicates.remove_duplicates_comprehension()), 11)
        self.assertListEqual(result, expected)
