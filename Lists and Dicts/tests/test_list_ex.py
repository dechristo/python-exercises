from src.base.list_ex import ListEx
import unittest


class ListExTest(unittest.TestCase):

    def test_is_empty_return_true_if_list_is_empty(self):
        empty = []
        list_ex = ListEx(empty)

        self.assertTrue(list_ex.is_empty())


    def test_is_empty_return_false_if_list_not_empty(self):
        not_empty = [1, 2, 3, 4]
        list_ex = ListEx(not_empty)

        self.assertFalse(ListEx(not_empty))
