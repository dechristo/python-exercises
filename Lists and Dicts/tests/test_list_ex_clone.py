from src.base.list_ex import ListEx
import unittest

class ListExCloneTest(unittest.TestCase):

    def test_clone_returns_cloned_integers_list(self):
        expected = [3,4,5,6,7,0,0,0]
        list_ex = ListEx(expected)
        cloned = list_ex.clone()

        self.assertEqual(expected, cloned)


    def test_clone_returns_cloned_strings_list(self):
        expected = ['this', 'is', 'a', 'unit', 'test']
        list_ex = ListEx(expected)
        cloned = list_ex.clone()

        self.assertEqual(expected, cloned)


    def test_clone_returns_cloned_list_of_lists(self):
        expected = [[3, 56, 2], ['a', 'abc'], [34, 'tre']]
        list_ex = ListEx(expected)
        cloned = list_ex.clone()

        self.assertEqual(expected, cloned)


    def test_clone_returns_cloned_list_of_dictionaries(self):
        expected = [{'a': 23, 'b': 34}, {'test' : 'unit'}, {'array':[]}]
        list_ex = ListEx(expected)
        cloned = list_ex.clone()

        self.assertEqual(expected, cloned)
