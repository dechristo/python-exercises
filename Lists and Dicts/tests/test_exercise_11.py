from exercise_11 import Exercise11
import unittest


class Exercise11Test(unittest.TestCase):
    def setUp(self):
        self.integer_collection = [5,6,1,44,234,9,1001]
        self.strings_collection = ["star", "wars", "jedi", "sith", "snoke"]
        self.exercise11 = Exercise11(self.integer_collection)

    def test_returns_true_if_sublist_has_one_element_in_common_for_integer_list(self):
        sub_collection = [99, -99, 0, 44, 777]
        result = self.exercise11.hasCommonElement(sub_collection)

        self.assertTrue(result)

        result = self.exercise11.hasCommonElementLC(sub_collection)

        self.assertTrue(result)

    def test_returns_false_if_sublist_has_no_elements_in_common_for_integer_list(self):
        sub_collection = [88, 87, 85, 500]
        result = self.exercise11.hasCommonElement(sub_collection)

        self.assertFalse(result)

        result = self.exercise11.hasCommonElementLC(sub_collection)

        self.assertFalse(result)

    def test_returns_true_if_sublist_has_one_element_in_common_for_string_list(self):
        sub_collection = ["test", "who", "is", "snoke"]
        self.exercise11 = Exercise11(self.strings_collection)
        result = self.exercise11.hasCommonElement(sub_collection)

        self.assertTrue(result)

        result = self.exercise11.hasCommonElementLC(sub_collection)

        self.assertTrue(result)

    def test_returns_false_if_sublist_has_no_elements_in_common_for_string_list(self):
        sub_collection = ["rey", "kyle", "luke"]
        result = self.exercise11.hasCommonElement(sub_collection)

        self.assertFalse(result)

        result = self.exercise11.hasCommonElementLC(sub_collection)

        self.assertFalse(result)
