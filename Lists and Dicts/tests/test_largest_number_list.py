import unittest
from src.base.list_ex import ListEx

class LargestNumberListTest(unittest.TestCase):

    def test_find_largest_on_positive_integers_list(self):
        ints = [3, 55, 9, 84, 34, 333, 12314, 0, 5, 145]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_largest(), max(ints))


    def test_find_largest_on_negative_integers_list(self):
        ints = [-3, -55, -9, -84, -34, -333, -12314, -5, -145]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_largest(), max(ints))


    def test_find_largest_on_equal_integers_list(self):
        ints = [3, 3, 3, 3, 3]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_largest(), max(ints))


    def test_find_largest_on_integers_list(self):
        ints = [-3, 55, 9, -84, 34, 333, -12314, 0, 5, 145]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_largest(), max(ints))

    def test_find_largest_on_positive_integers_list_recursively(self):
        ints = [3, 55, 9, 84, 34, 333, 12314, 0, 5, 145]
        list_ex = ListEx(ints)

        self.assertEqual(list_ex.find_largest_recursive(ints[0], 1), max(ints))
