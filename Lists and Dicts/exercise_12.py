class Exercise12:

    def __init__(self):
        self.indexes_to_remove = (0, 4, 5)

    def remove_elements(self, collection):
        if len(collection) < max(self.indexes_to_remove):
            return None

        result = [x for (i, x) in enumerate(collection) if i not in (0 ,4, 5)]
        return result
