class Exercise10:

    def __init__(self, collection):
        self.words = collection


    def greater_than(self, length):
        result = [word for word in self.words if len(word) > length]
        return result


    def greater_than_filter(self,  length):
        result = filter((lambda x: len(x) > length), self.words)
        return result
