objects = []

obj1 = {
    'name' : 'a',
    'score_1' : 10,
    'score_2' : 10,
    'score_3' : 10
}
objects.append(obj1)

obj2 = {
    'name' : 'b',
    'score_1' : 8,
    'score_2' : 7,
    'score_3' : 8
}
objects.append(obj2)

obj3 = {
    'name' : 'c',
    'score_1' : 10,
    'score_2' : 4,
    'score_3' : 9
}
objects.append(obj3)

obj4 = {
    'name' : 'd',
    'score_1' : 7,
    'score_2' : 7,
    'score_3' : 7
}
objects.append(obj4)

sorted_objects = sorted(objects, key = lambda v: (v['score_1'] + v['score_2'] + v['score_3'])/3)
print(sorted_objects)

