def extract_repetitions(sequence):
	previous = sequence[0]
	count = 1

	for char in sequence[1:]:
		if char == previous:
			count += 1
			continue
		
		print(count, previous)
		count = 1
		previous = char

	print(count, sequence[-1])


def extract_repetitions_recursively(sequence, previous, index, count):
	if(index == len(sequence) - 1):
		print(count+1, sequence[-1])
		return
	
	if previous == sequence[index]:
		count +=1;
	else:
	   print(count, previous)
	   count = 1
	   previous = sequence[index]

	extract_repetitions_recursively(sequence, previous, index + 1, count)


def extract_repetitions_recursively_ex(sequence, previous='', count=0):		
	
	if not previous:
		return extract_repetitions_recursively_ex(sequence[1:], sequence[0], 1)

	if not sequence:
		print(count, previous)
		return

	if (sequence[0] == previous): 
		count += 1
	else:
		print(count, previous)
		count = 1

	extract_repetitions_recursively_ex(sequence[1:], sequence[0], count)

#extract_repetitions('12')	
#extract_repetitions('2222222')
extract_repetitions_recursively_ex('7771444488888888')
print('-------------')
extract_repetitions_recursively_ex('148333')
print('-------------')
extract_repetitions_recursively_ex('')

