
def fib(n):
    if n <= 1:
        return n

    previous_1 = 1
    previous_2 = 0
    current = 1

    for x in range(2, n):
        current = previous_1 + previous_2
        previous_2 = previous_1
        previous_1 = current

    return current

def fib_recursive(n):
    if n <= 1:
        return n
    current = fib_recursive(n-1) + fib_recursive(n-2)
    return current

n = 0
while n != 9999:
    n = int(input())
    #print (fib(n))
    print (fib_recursive(n))





