''' Class for educational purposes regarding python lists.
    Of course, most of the methods implemented here
    already have a built-in implementation.'''
from functools import reduce


class ListEx(list):

    def __init__(self, collection):
        super(ListEx, self).__init__(self)
        self.collection = collection


    def sum_reduce_lambda(self):
        list_sum = reduce((lambda x, y: x + y), self.collection)
        return list_sum


    def sum_reduce(self):
        list_sum = reduce(self.__sum, self.collection)
        return list_sum


    def sum_recursive(self, list_sum, index):
        if index + 1 == len(self.collection):
            return self.collection[index]

        list_sum = self.collection[index] + self.sum_recursive(list_sum, index + 1)
        return list_sum


    def multiply_reduce_lambda(self):
        list_multiply = reduce((lambda x, y: x * y), self.collection)
        return list_multiply


    def multiply_reduce(self):
        list_multiply = reduce(self.__multiply, self.collection)
        return list_multiply


    def multiply_recursive(self, multiplied, index):
        if index + 1 == len(self.collection):
            return self.collection[index]

        multiplied = self.collection[index] * self.multiply_recursive(multiplied, index + 1)
        return multiplied


    def find_largest(self):
        largest = self.collection[0]
        for x in self.collection[1:]:
            if x > largest:
                largest = x

        return largest


    def find_largest_recursive(self, largest, index):
        if index + 1 == len(self.collection):
            return largest

        if largest < self.collection[index]:
            largest = self.collection[index]

        largest = self.find_largest_recursive(largest, index + 1)
        return largest


    def find_smallest(self):
        smallest = self.collection[0]
        for x in self.collection[1:]:
            if x < smallest:
                smallest = x

        return smallest


    def find_smallest_recursive(self, smallest, index):
        if index + 1 == len(self.collection):
            return smallest

        if smallest > self.collection[index]:
            smallest = self.collection[index]

        smallest = self.find_smallest_recursive(smallest, index + 1)
        return smallest


    def clone(self):
        return self.collection.copy()


    def is_empty(self):
        return not self.collection

    def __sum(self, x, y):
        return x + y


    def __multiply(self, x, y):
        return x * y
