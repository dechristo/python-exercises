class FibonacciRecursive:

    @staticmethod
    def get_term(index):
        if index <= 1:
            return index

        return FibonacciRecursive.get_term(index - 1) + FibonacciRecursive.get_term(index - 2)
