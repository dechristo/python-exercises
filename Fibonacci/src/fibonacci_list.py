class FibonacciList:
    def __init__(self):
        self.sequence = [0, 1]

    def get_term(self, index):
        if index <= 1:
            return self.sequence[index]

        for term in range(2, index+1):
            self.sequence.append(self.sequence[-1] + self.sequence[-2])

        return self.sequence[index]
