class Fibonacci:

    @staticmethod
    def get_term(index):
        if index <= 1:
            return index

        current = 1
        previous = 0
        result = 0

        for term in range(2, index+1):
            result = current + previous
            temp = current
            current = result
            previous = temp

        return result
