import unittest
from src.fibonacci import Fibonacci


class FibonacciListTest(unittest.TestCase):
    def setUp(self):
        self.sequence = [0, 1]
        self.fibonacci = Fibonacci()


    def test_returns_correct_value_for_first_two_terms(self):
        first = self.fibonacci.get_term(0)
        second = self.fibonacci.get_term(1)

        self.assertEqual(0, first)
        self.assertEqual(1, second)


    def test_returns_correct_value_for_third_term(self):
        result = self.fibonacci.get_term(2)

        self.assertEqual(1, result)


    def test_returns_correct_value_for_fourth_term(self):
        result = self.fibonacci.get_term(3)

        self.assertEqual(2, result)


    def test_returns_correct_value_for_fifth_term(self):
        result = self.fibonacci.get_term(4)

        self.assertEqual(3, result)


    def test_returns_correct_value_for_sixth_term(self):
        result = self.fibonacci.get_term(5)

        self.assertEqual(5, result)


    def test_returns_correct_value_for_seventh_term(self):
        result = self.fibonacci.get_term(6)

        self.assertEqual(8, result)


    def test_returns_correct_value_for_eighth_to_hundredth_term(self):
        for index in range(7, 100):
            result = self.fibonacci.get_term(index)
            self.assertEqual(self.fibonacci.get_term(index - 1) + self.fibonacci.get_term(index - 2), result)
