import unittest
from src.fibonacci_recursive import FibonacciRecursive


class FibonacciRecursiveTest(unittest.TestCase):

        def setUp(self):
            self.sequence = [0, 1]


        def test_returns_correct_value_for_first_two_terms(self):
            first = FibonacciRecursive.get_term(0)
            second = FibonacciRecursive.get_term(1)

            self.assertEqual(0, first)
            self.assertEqual(1, second)

        def test_returns_correct_value_for_third_term(self):
            result = FibonacciRecursive.get_term(2)

            self.assertEqual(1, result)

        def test_returns_correct_value_for_fourth_term(self):
            result = FibonacciRecursive.get_term(3)

            self.assertEqual(2, result)

        def test_returns_correct_value_for_fifth_term(self):
            result = FibonacciRecursive.get_term(4)

            self.assertEqual(3, result)

        def test_returns_correct_value_for_sixth_term(self):
            result = FibonacciRecursive.get_term(5)

            self.assertEqual(5, result)

        def test_returns_correct_value_for_seventh_term(self):
            result = FibonacciRecursive.get_term(6)

            self.assertEqual(8, result)

        def test_returns_correct_value_for_eighth_to_third_fourth_term(self):
            for index in range(7, 34):
                result = FibonacciRecursive.get_term(index)
                self.assertEqual(FibonacciRecursive.get_term(index - 1) + FibonacciRecursive.get_term(index - 2), result)

