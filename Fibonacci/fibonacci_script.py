from src.fibonacci import Fibonacci


def exec():
	print('Tell me a number:')
	limit = int(input())
	
	fibonacci = Fibonacci()
	result = fibonacci.get_term(limit)
	
	print('The Fibonacci element for index {0} is {1}'.format(limit, result))

exec()
