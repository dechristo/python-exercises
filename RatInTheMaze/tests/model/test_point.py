import unittest
from src.model.point import Point


class PointTest(unittest.TestCase):
    def test_constructor_returns_point_object(self):
        point = Point()

        self.assertIsInstance(point, Point)


    def test_constructor_with_no_parameters_returns_object_with_default_coordinate(self):
        point = Point()

        self.assertEqual(point.X, 0)
        self.assertEqual(point.Y, 0)


    def test_constructor_returns_object_with_provided_equal_positive_coordinates(self):
        point = Point(3,3)

        self.assertEqual(3, point.X)
        self.assertEqual(3, point.Y)


    def test_constructor_returns_object_with_provided_different_positive_coordinates(self):
        point = Point(12,7)

        self.assertEqual(12, point.X)
        self.assertEqual(7, point.Y)


    def test_constructor_raise_exception_if_x_is_not_integer(self):
        self.assertRaises(ValueError, Point, 'ahoy', 2)
        self.assertRaises(ValueError, Point, 23.780, 2)


    def test_constructor_raise_exception_if_y_is_not_integer(self):
        self.assertRaises(ValueError, Point, 5, 'wow')
        self.assertRaises(ValueError, Point, 4, 2.22)

