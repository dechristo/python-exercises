import unittest
from src.model.maze import Maze
from src.model.point import Point

class MazeTest(unittest.TestCase):

    def setUp(self):
        self.maze_map =  [[1,  0,  0,  0],
                     [1,  1,  0,  0],
                     [0,  1,  0,  0 ],
                     [0,  1,  1,  1]]

        self.maze = Maze(self.maze_map)


    def tearDown(self):
        self.maze = None

    def test_constructor_returns_point_object(self):
        self.assertIsInstance(self.maze, Maze)


    def test_constructor_with_default_parameters_sets_correct_maze_size(self):
        self.assertEqual((4,4), self.maze.size)
        self.assertEqual(4, self.maze.lines)
        self.assertEqual(4, self.maze.columns)


    def test_constructor_sets_correct_maze_size(self):
        maze_map_10x6 = [[0] * 10 for x in range(6)]
        maze = Maze(maze_map_10x6, 10, 6)

        self.assertEqual((10,6), maze.size)
        self.assertEqual(10, maze.lines)
        self.assertEqual(6, maze.columns)


    def test_is_safe_returns_false_if_x_is_lower_than_zero(self):
        self.assertFalse(self.maze.is_index_safe(Point(-2,7)))


    def test_is_safe_returns_false_if_y_is_lower_than_zero(self):
        self.assertFalse(self.maze.is_index_safe(Point(4,-1)))


    def test_is_safe_returns_false_if_x_is_greater_than_or_equal_size_x(self):
        self.assertFalse(self.maze.is_index_safe(Point(4, 2)))
        self.assertFalse(self.maze.is_index_safe(Point(9, 2)))


    def test_is_safe_returns_false_if_y_is_greater_than_or_equal_size_y(self):
        self.assertFalse(self.maze.is_index_safe(Point(1, 7)))
        self.assertFalse(self.maze.is_index_safe(Point(0, 4)))

    def test_is_safe_returns_true_if_coordinate_is_safe(self):
        maze_ones = [[1] * 4 for x in range(4)]
        self.maze = Maze(maze_ones)
        self.assertTrue(self.maze.is_index_safe(Point(0, 0)))
        self.assertTrue(self.maze.is_index_safe(Point(1, 1)))
        self.assertTrue(self.maze.is_index_safe(Point(2, 2)))
        self.assertTrue(self.maze.is_index_safe(Point(3, 3)))

    def test_returns_true_for_valid_maze_4x4_1(self):
        self.assertTrue(self.maze.solve())
