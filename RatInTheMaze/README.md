Solves the Maze problem:
Beginning in the top left corner, how many possible paths can you have to the exit at bottom right?
