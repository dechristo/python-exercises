from src.model.point import Point


class Maze():

    def __init__(self, maze, m = 4, n = 4):
        self.size = (m, n)
        self.lines = m
        self.columns = n
        self.maze = maze

    def get_size(self):
        return self.size


    def is_index_safe(self, point):
        if self.__is_x_inside_bounds(point.X) and \
           self.__is_y_inside_bounds(point.Y) and \
           self.maze[point.X][point.Y] == 1:
           return True

        return False


    def solve(self):
        solution_path = [[0] * self.lines for cols in range(self.columns)]

        if not self.solve_aux(self.maze, 0, 0, solution_path):
            print("No paths found!")
            return False

        self.print(solution_path)
        return True


    def solve_aux(self, maze, x, y, path):
        if x == self.lines - 1 and \
            y == self.columns - 1 and \
            self.is_index_safe(Point(x,y)):
                path[x][y] = 1
                return True

        if self.is_index_safe(Point(x, y)):
            path[x][y] = 1

            # go right
            if self.solve_aux(maze, x + 1, y, path):
                return True

            #go down
            if self.solve_aux(maze, x, y + 1, path):
                return True

            path[x][y] = 0
            return False

        return False


    def print(self, s):
        pass


   # def __initMaze(self):
       # self.maze = [[0 for m in range(self.size[0])] for n in range(self.size[1])]


    def __is_x_inside_bounds(self, x):
        return x >= 0 and x < self.lines


    def __is_y_inside_bounds(self, y):
        return y >= 0 and y < self.columns
