class Point:

    def __init__(self, x = 0, y = 0):
        if not isinstance(x, int):
            raise ValueError('X need to be an integer!')

        if not isinstance(y, int):
            raise ValueError('Y need to be an integer!')

        self.X = x
        self.Y = y
