from model.balance import Balance
'''Script to check if a given sequence is balance or not.'''

sequence = input()
result = Balance.is_balanced(sequence)
if result:
    print('Yay! Sequence is balanced!!!!')
else:
    print('Ooops! Sequence is NOT balanced!!!!')
