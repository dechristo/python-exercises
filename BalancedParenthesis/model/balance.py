class Balance:

    @staticmethod
    def get_valid_elements():
        return ['(', ')', '[', ']', '{', '}']

    @staticmethod
    def is_balanced(sequence):
        if not sequence:
            return True

        stack = []

        for character in sequence:
            if character not in Balance.get_valid_elements():
                continue

            if character in ['(', '[', '{']:
                stack.append(character)
                continue

            if not stack or not Balance.__match(character, stack[-1]):
                return False

            stack.pop()

        return not stack


    @staticmethod
    def __match(character, stack_top):
        if character == ')' and stack_top == '(': return True
        if character == ']' and stack_top == '[': return True
        if character == '}' and stack_top == '{': return True

        return False


