import unittest
from model.balance import Balance

class BalanceTest(unittest.TestCase):

    def test_get_valid_elements_returns_array_with_valid_elements(self):
        elements = Balance.get_valid_elements()

        self.assertIsInstance(elements, list)
        self.assertIn('(', elements)
        self.assertIn(')', elements)
        self.assertIn('[', elements)
        self.assertIn(']', elements)
        self.assertIn('{', elements)
        self.assertIn('}', elements)

    def test_returns_true_for_empty_sequence(self):
        result = Balance.is_balanced('')

        self.assertTrue(result)


    def test_returns_true_for_basic_parenthesis_valid_test_case(self):
        sequence = '()'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_false_for_basic_parenthesis_invalid_test_case(self):
        sequence = ')('
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_false_for_only_one_opening_parenthesis_invalid_test_case(self):
        sequence = '('
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_false_for_only_one_closing_parenthesis_invalid_test_case(self):
        sequence = ')'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_true_for_basic_nested_parenthesis_valid_test_case(self):
        sequence = '(())'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_3_parenthesis_valid_test_case(self):
        sequence = '((()))'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_4_parenthesis_valid_test_case(self):
        sequence = '(((())))'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_group_of_parenthesis_valid_test_case(self):
        sequence = '(()())'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_deeply_nested_group_of_parenthesis_valid_test_case(self):
        sequence = '((()(()))())'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_brackets_valid_test_case(self):
        sequence = '[]'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_false_for_basic_bracket_invalid_test_case(self):
        sequence = ']['
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_false_for_only_one_opening_bracket_invalid_test_case(self):
        sequence = '['
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_false_for_only_one_closing_bracket_invalid_test_case(self):
        sequence = ']'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_true_for_basic_nested_brackets_valid_test_case(self):
        sequence = '[[]]'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_3_brackets_valid_test_case(self):
        sequence = '[[[]]]'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_4_brackets_valid_test_case(self):
        sequence = '[[[[]]]]'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_group_of_brackets_valid_test_case(self):
        sequence = '[[][]]'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_deeply_nested_group_of_brackets_valid_test_case(self):
        sequence = '[[[][[]]][]]'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_curly_braces_valid_test_case(self):
        sequence = '{}'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)

    def test_returns_false_for_basic_curly_braces_invalid_test_case(self):
        sequence = '}{'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)

    def test_returns_false_for_only_one_opening_curly_braces_invalid_test_case(self):
        sequence = '{'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)

    def test_returns_false_for_only_one_closing_curly_braces_invalid_test_case(self):
        sequence = '}'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)

    def test_returns_true_for_basic_nested_curly_braces_valid_test_case(self):
        sequence = '{{}}'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_3_curly_braces_valid_test_case(self):
        sequence = '{{{}}}'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_4_curly_braces_valid_test_case(self):
        sequence = '{{{{}}}}'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_nested_group_of_curly_braces_valid_test_case(self):
        sequence = '{{}{}}'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_basic_deeply_nested_group_of_curly_braces_valid_test_case(self):
        sequence = '{{{}{{}}}{}}'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_mixed_1_valid(self):
        sequence = '{[()]}'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_mixed_2_valid(self):
        sequence = '(){[][()]}{{[()]}[]}'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_true_for_mixed_3_valid(self):
        sequence = '({}{}{}[[()]]{{{[]()((()))}[]{}}})'
        result = Balance.is_balanced(sequence)

        self.assertTrue(result)


    def test_returns_false_for_mixed_1_invalid(self):
        sequence = '{[())]}'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_false_for_mixed_2_invalid(self):
        sequence = '(){[][[()]}{{[()]}[]}'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_false_for_mixed_3_invalid(self):
        sequence = '({}{}{}[[()]]({{{[]()((()))}[]){}}[})'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_false_for_mixed_4_invalid(self):
        sequence = '())'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)


    def test_returns_false_for_mixed_5_invalid(self):
        sequence = '([)]'
        result = Balance.is_balanced(sequence)

        self.assertFalse(result)
